﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

namespace de.jniederm.bachelor.player.VR
{
    public class VRController : MonoBehaviour
    {

        public float TriggerAxis = 0f;
        public float TouchpadX = 0f;
        public float TouchpadY = 0f;
        public float GripAxis = 0f;
        public bool MenuButton = false;
        public bool GripButton = false;
        public bool TriggerButton = false;
        public bool TouchpadClick = false;
        public bool TouchpadTouch = false;
        public float JoystickX = 0f;
        public float JoystickY = 0f;


        // Start is called before the first frame update
        public virtual void Start()
        {

        }

        // Update is called once per frame
        public virtual void Update()
        {

        }
    }
}
