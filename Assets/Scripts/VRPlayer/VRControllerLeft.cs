﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace de.jniederm.bachelor.player.VR
{
    public class VRControllerLeft : VRController
    {
        

        // Start is called before the first frame update
        public override void Start()
        {
            base.Start();
        }

        // Update is called once per frame
        public override void Update()
        {
            base.Update();
            TriggerAxis = Input.GetAxis("LeftTrigger");
            GripAxis = Input.GetAxis("LeftGripAxis");
            GripButton = Input.GetButton("LeftGripButton");
            TriggerButton = Input.GetButton("LeftTriggerButton");
            TouchpadClick = Input.GetButton("LeftPrimary2DAxisClick");
            TouchpadTouch = Input.GetButton("LeftPrimary2DAxisTouch");

            string[] joysticks = Input.GetJoystickNames();
            if (joysticks.Length >= 1 && joysticks[0].Contains("WindowsMR"))
            {
                TouchpadX = Input.GetAxis("LeftSecondary2DAxisX");
                TouchpadY = Input.GetAxis("LeftSecondary2DAxisY");
                JoystickX = Input.GetAxis("LeftPrimary2DAxisX"); 
                JoystickY = Input.GetAxis("LeftPrimary2DAxisY"); 
            }
            else
            {
                TouchpadX = Input.GetAxis("LeftPrimary2DAxisX");
                TouchpadY = Input.GetAxis("LeftPrimary2DAxisY");
                JoystickX = Input.GetAxis("LeftSecondary2DAxisX"); 
                JoystickY = Input.GetAxis("LeftSecondary2DAxisY"); 
            }
            
        }
    }
}
