﻿using UnityEngine;
using UnityEngine.SpatialTracking;
using UnityEngine.UI;
using UnityEngine.XR;
using static UnityEngine.SpatialTracking.TrackedPoseDriver;

namespace de.jniederm.bachelor.player.VR
{
    public class VRPointer : MonoBehaviour
    {
        public LineRenderer line = null;
        public VRController controller = null;
        private bool buttonReset = true;
        private Button selected = null;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, 5f))
            {
                line.SetPosition(1, new Vector3(0, 0, 1) * hit.distance);
                GameObject obj = hit.collider.gameObject;
                if (obj.CompareTag("Button"))
                {
                    SendHaptic();
                    // aktiviert highlight für button
                    if(selected != null)
                    {
                        selected.OnDeselect(null);
                    }
                    selected = obj.GetComponent<Button>();
                    selected.OnSelect(null);

                    // löst funktion des button aus
                    if (controller.TriggerButton && buttonReset)
                    {
                        buttonReset = false;
                        selected.onClick.Invoke();
                    }
                    else if (!buttonReset && !!controller.TriggerButton)
                    {
                        buttonReset = true;
                    }
                }
                else
                {
                    buttonReset = true;
                }
            }
            else
            {
                line.SetPosition(1, new Vector3(0, 0, 3));
                if (selected != null)
                {
                    selected.OnDeselect(null);
                    selected = null;
                }
            }
        }

        // sendet haptisches feedback
        private void SendHaptic()
        {
            InputDevice device;
            if (GetComponentInParent<TrackedPoseDriver>().poseSource == TrackedPose.LeftPose)
            {
                device = InputDevices.GetDeviceAtXRNode(XRNode.LeftHand);
            }
            else if (GetComponentInParent<TrackedPoseDriver>().poseSource == TrackedPose.RightPose)
            {
                device = InputDevices.GetDeviceAtXRNode(XRNode.RightHand);
            }
            else
            {
                device = InputDevices.GetDeviceAtXRNode(XRNode.GameController);
            }
            if (device != InputDevices.GetDeviceAtXRNode(XRNode.GameController) && device.TryGetHapticCapabilities(out HapticCapabilities capabilities))
            {
                if (capabilities.supportsImpulse)
                {
                    uint channel = 0;
                    float amplitude = 0.1f;
                    float duration = 2f;
                    device.SendHapticImpulse(channel, amplitude, duration);
                }
            }
        }

        private void OnDisable()
        {
            line.enabled = false;
        }

        private void OnEnable()
        {
            line.enabled = true;
        }
    }

}