﻿using de.jniederm.bachelor.game;
using de.jniederm.bachelor.game.data;
using de.jniederm.bachelor.placeable;
using TMPro;
using UnityEngine;

namespace de.jniederm.bachelor.player.VR
{
    public class BlockPlacementController : MonoBehaviour
    {
        public GameObject Selected = null;
        public Canvas PlaceableList = null;
        public float MaxDistance = 5f;
        public TextMeshProUGUI textMesh;

        private bool ButtonReset = true;
        private int selectedIndex = 0;
        private int maxIndex;
        private PlaceableLibrary library;

        public VRController controller = null;

        // Start is called before the first frame update
        void Start()
        {
            library = LevelManager.instance.Library;
            maxIndex = library.Count;
            Selected = library[selectedIndex].placeable.gameObject;
            textMesh.SetText(library[selectedIndex].name);
        }

        // Update is called once per frame
        void Update()
        {
            RegisterButtonPresses();
        }

        // Registriert, ob bestimmte knöpfe gedrückt wurden und führt dementsprechende aktionen aus.
        private void RegisterButtonPresses()
        {
            //check, ob ein Knopf gedrückt wurde. wenn nicht, setze den Reset zurück
            if (!ButtonReset && !controller.TouchpadClick && !controller.TriggerButton && !controller.GripButton)
            {
                ButtonReset = true;
            }
            if (ButtonReset)
            {
                // überprüft, welcher knopf gedürckt wurde.
                if (controller.TouchpadClick)
                {
                    TouchpadClicked();
                }
                else if (controller.TriggerButton)
                {
                    TriggerPressed();
                }
                else if (controller.GripButton)
                {
                    GripPressed();
                }

            }
        }

        // funktion für grifftaste. löscht das anvisierte element
        private void GripPressed()
        {
            ButtonReset = false;
            if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, MaxDistance))
            {
                if (hit.rigidbody.CompareTag("Conveyor") || hit.rigidbody.CompareTag("Sensor") || hit.rigidbody.CompareTag("Cable") || hit.rigidbody.CompareTag("Piston") || hit.rigidbody.CompareTag("RotatorLeft") || hit.rigidbody.CompareTag("RotatorRight") || hit.rigidbody.CompareTag("Welder") || hit.rigidbody.CompareTag("IdleBlock"))
                {
                    var obj = hit.rigidbody.gameObject.GetComponent<PlaceableElement>();
                    if (obj != null && !obj.LevelObject)
                    {
                        obj.Remove();
                    }
                }
            }
        }

        // überprüft, ob der Trigger gedrückt wurde. instanziiert neues element.
        private void TriggerPressed()
        {
            ButtonReset = false;
            if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, MaxDistance))
            {
                if (hit.collider.CompareTag("Ground") || hit.collider.CompareTag("IdleBlock"))
                {
                    var position = new Vector3(Mathf.Round(hit.point.x), Mathf.Round(hit.point.y), Mathf.Round(hit.point.z));
                    if(Physics.OverlapSphere(new Vector3(position.x, position.y + 0.5f, position.z), 0.1f).Length <= 0)
                    {
                        if (Selected.CompareTag("Cable")) 
                        {
                            var tmp = Instantiate(Selected, new Vector3(position.x, position.y + 0.5f, position.z), Quaternion.identity);
                            Debug.Log("created " + tmp.ToString());

                        }
                        else
                        {
                            var tmp = Instantiate(Selected, position, Quaternion.identity);
                            Debug.Log("created " + tmp.ToString());
                        }
                        
                    }
                }
            }
        }

        
        private void TouchpadClicked()
        {
            // überprüfung, ob auf die obere seite des Touchpads gedrückt wurde
            if (controller.TouchpadY >= 0.7f)
            {
                ButtonReset = false;
                if (selectedIndex < maxIndex - 1)
                {
                    selectedIndex += 1;
                }
                else
                {
                    selectedIndex = 0;
                }
                Selected = library[selectedIndex].placeable.gameObject;
                textMesh.SetText(library[selectedIndex].name);
            }
            // überprüfung, ob auf die untere seite des Touchpads gedrückt wurde
            else if (controller.TouchpadY <= -0.7f)
            {
                ButtonReset = false;
                if (selectedIndex > 0)
                {
                    selectedIndex -= 1;
                }
                else
                {
                    selectedIndex = maxIndex - 1;
                }
                Selected = library[selectedIndex].placeable.gameObject;
                textMesh.SetText(library[selectedIndex].name);
            }
        }


        // Diese Funktion wird aufgerufen, wenn das Objekt aktiviert wird und aktiv ist
        private void OnEnable()
        {
            PlaceableList.gameObject.SetActive(true);
        }

        // Diese Funktion wird aufgerufen, wenn "Behaviour" deaktiviert wird oder inaktiv ist
        private void OnDisable()
        {
            PlaceableList.gameObject.SetActive(false);
        }
    }
}
