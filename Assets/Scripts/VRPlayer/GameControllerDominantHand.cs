﻿using de.jniederm.bachelor.game;
using UnityEngine;
using UnityEngine.SpatialTracking;

namespace de.jniederm.bachelor.player.VR
{
    public class GameControllerDominantHand : VRControllerRight
    {
        private LineRenderer line;
        public MovementController move;
        public BlockEditController edit;
        public BlockPlacementController place;
        public TrackedPoseDriver dominant;
        public TrackedPoseDriver offHand;

        // Start is called before the first frame update
        public override void Start()
        {
            base.Start();
            line = GetComponent<LineRenderer>();
            move.enabled = false;
            edit.enabled = false;
            place.enabled = false;
        }

        // Update is called once per frame
        public override void Update()
        {
            base.Update();
        }

        public void EnableMovementMode()
        {
            move.enabled = true;
            edit.enabled = false;
            place.enabled = false;
            line.material.color = Color.yellow;
        }

        public void EnableBuildMode()
        {
            move.enabled = false;
            edit.enabled = false;
            place.enabled = true;
            line.material.color = Color.green;
        }

        public void EnableEditMode()
        {
            move.enabled = false;
            edit.enabled = true;
            place.enabled = false;
            line.material.color = Color.blue;
        }

        public void ResetLevelClicked()
        {
            LevelManager.instance.ResetLevel();
        }

        public void SwitchHandClicked()
        {
            if (dominant.poseSource == TrackedPoseDriver.TrackedPose.RightPose)
            {
                dominant.SetPoseSource(TrackedPoseDriver.DeviceType.GenericXRController, TrackedPoseDriver.TrackedPose.LeftPose);
                offHand.SetPoseSource(TrackedPoseDriver.DeviceType.GenericXRController, TrackedPoseDriver.TrackedPose.RightPose);
            }
            else if (dominant.poseSource == TrackedPoseDriver.TrackedPose.LeftPose)
            {
                dominant.SetPoseSource(TrackedPoseDriver.DeviceType.GenericXRController, TrackedPoseDriver.TrackedPose.RightPose);
                offHand.SetPoseSource(TrackedPoseDriver.DeviceType.GenericXRController, TrackedPoseDriver.TrackedPose.LeftPose);
            }
        }
    }
}
