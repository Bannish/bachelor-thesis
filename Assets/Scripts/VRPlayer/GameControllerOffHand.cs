﻿using de.jniederm.bachelor.game;
using UnityEngine;
using UnityEngine.UI;

namespace de.jniederm.bachelor.player.VR
{
    public class GameControllerOffHand : VRControllerLeft
    {

        public Canvas ControlMenu;
        public RawImage PlayButton;

        private bool ButtonReset { get; set; }
        private bool MenuActive { get; set; } = false;

        // Start is called before the first frame update
        public override void Start()
        {
            base.Start();
        }

        // Update is called once per frame
        public override void Update()
        {
            base.Update();
            ShowMenu();
        }

        // Aktiviert oder Deaktiviert das Menü je nach Rotation des Controllers.
        private void ShowMenu()
        {
            if (transform.localRotation.eulerAngles.z <= 210 && transform.localRotation.eulerAngles.z >= 90)
            {
                ControlMenu.gameObject.SetActive(true);
            }
            else
            {
                ControlMenu.gameObject.SetActive(false);
            }
        }

        // Diese Funktion wird aufgerufen, wenn das Objekt aktiviert wird und aktiv ist
        private void OnEnable()
        {
            LevelManager.OnRunningChanged += ChangePlayButtonColor;
        }

        // Diese Funktion wird aufgerufen, wenn "Behaviour" deaktiviert wird oder inaktiv ist
        private void OnDisable()
        {
            LevelManager.OnRunningChanged -= ChangePlayButtonColor;
        }

        // Ändert die Farbe des Playbutton je nach PlayMode.
        private void ChangePlayButtonColor()
        {
            PlayButton.color = LevelManager.instance.Running ? Color.green : Color.white;
        }
    }
}
