﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace de.jniederm.bachelor.player.VR
{
    public class MovementController : MonoBehaviour
    {
        public float MaxDistance = 5f;
        public VRController controller = null;
        private bool ButtonReset = true;

        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, MaxDistance))
            {
                TeleportPlayer(hit);
            }
            if (controller.TouchpadClick && ButtonReset)
                ConvenienceRotatePlayer();

            if (!controller.TriggerButton && !controller.TouchpadClick)
            {
                ButtonReset = true;
            }
        }

        private void TeleportPlayer(RaycastHit hit)
        {
            if (hit.collider.CompareTag("Ground") || hit.collider.CompareTag("IdleBlock"))
            {

                if (controller.TriggerButton && ButtonReset)
                {
                    ButtonReset = false;
                    gameObject.transform.root.position = hit.point;
                }
            }
        }

        private void ConvenienceRotatePlayer()
        {
            ButtonReset = false;
            if (controller.TouchpadX > 0.7)
            {
                gameObject.transform.root.rotation = gameObject.transform.root.rotation * Quaternion.Euler(0f, 45f, 0f);
            }
            else if (controller.TouchpadX < -0.7)
            {
                gameObject.transform.root.rotation = gameObject.transform.root.rotation * Quaternion.Euler(0f, -45f, 0f);
            }
        }
    }
}
