﻿using UnityEngine;

namespace de.jniederm.bachelor.player.VR
{
    public class VRControllerRight : VRController
    {
        // Start is called before the first frame update
        public override void Start()
        {
            base.Start();
        }

        // Update is called once per frame
        public override void Update()
        {
            base.Update();
            TriggerAxis = Input.GetAxis("RightTrigger");
            GripAxis = Input.GetAxis("RightGripAxis");
            GripButton = Input.GetButton("RightGripButton");
            TriggerButton = Input.GetButton("RightTriggerButton");
            TouchpadClick = Input.GetButton("RightPrimary2DAxisClick");
            TouchpadTouch = Input.GetButton("RightPrimary2DAxisTouch");

            string[] joysticks = Input.GetJoystickNames();
            if (joysticks.Length >= 1 && joysticks[0].Contains("WindowsMR"))
            {
                TouchpadX = Input.GetAxis("RightSecondary2DAxisX");
                TouchpadY = Input.GetAxis("RightSecondary2DAxisY");
                JoystickX = Input.GetAxis("RightPrimary2DAxisX");
                JoystickY = Input.GetAxis("RightPrimary2DAxisY");
            }
            else
            {
                TouchpadX = Input.GetAxis("RightPrimary2DAxisX");
                TouchpadY = Input.GetAxis("RightPrimary2DAxisY");
                JoystickX = Input.GetAxis("RightSecondary2DAxisX");
                JoystickY = Input.GetAxis("RightSecondary2DAxisY");
            }
        }
    }
}
