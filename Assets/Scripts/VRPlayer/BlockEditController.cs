﻿using UnityEngine;

namespace de.jniederm.bachelor.player.VR
{
    public class BlockEditController : MonoBehaviour
    {

        public VRController controller = null;
        public GameObject selected = null;
        private bool ButtonReset = true;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, 5f))
            {
                if (ButtonReset && controller.TriggerButton)
                    TriggerPressed(hit);
                else if (ButtonReset && controller.TouchpadClick && selected != null)
                    TouchpadPressed();
                else if (ButtonReset && controller.GripButton && selected != null)
                    GripPressed();
                else if (!ButtonReset && !controller.TriggerButton && !controller.TouchpadClick && !controller.GripButton)
                {
                    ButtonReset = true;
                }
            }
        }

        // Funktion für die Grifftaste. löscht ausgewähltes objekt
        private void GripPressed()
        {
            var obj = selected.GetComponent<placeable.PlaceableElement>();
            if (obj != null && !obj.LevelObject)
            {

                obj.Remove();
                selected = null;
            }
        }

        // funktion für touchpad. drehen oder zurücksetzen
        private void TouchpadPressed()
        {
            ButtonReset = false;
            if (controller.TouchpadX < -0.6f)
            {
                selected.transform.Rotate(new Vector3(0, -90, 0), Space.Self);
            }
            else if (controller.TouchpadX > 0.6f)
            {
                selected.transform.Rotate(new Vector3(0, 90, 0), Space.Self);
            }
            else
            {
                selected.transform.rotation = Quaternion.Euler(0, 0, 0);
                selected.transform.position = new Vector3(selected.transform.position.x, 0, selected.transform.position.z);
            }
        }

        // funktion für trigger. Auswählen des objektes
        private void TriggerPressed(RaycastHit hit)
        {
            ButtonReset = false;
            if (hit.rigidbody == null)
            {
                selected = null;
            }
            else
            {
                selected = hit.rigidbody.CompareTag("Conveyor") || hit.rigidbody.CompareTag("Sensor") || hit.rigidbody.CompareTag("Cable") || hit.rigidbody.CompareTag("Piston") || hit.rigidbody.CompareTag("RotatorLeft") || hit.rigidbody.CompareTag("RotatorRight") || hit.rigidbody.CompareTag("Welder")
                    ? hit.rigidbody.gameObject
                    : (GameObject)null;
            }
        }
    }
}
