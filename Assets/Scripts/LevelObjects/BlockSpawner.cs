﻿using UnityEngine;

namespace de.jniederm.bachelor.game
{
    public class BlockSpawner : MonoBehaviour
    {

        public Transform SpawnPosition;
        public GameObject SpawnObject;
        public float MaxCooldown = 7f;
        private float cooldown = 0;
        private bool spawnable = true;

        // Start is called before the first frame update
        void Start()
        {
            LevelManager.instance.AddSpawner(gameObject);
            MaxCooldown = LevelManager.instance.SpawnDelay;
        }

        // Update is called once per frame
        void Update()
        {
            if(LevelManager.instance.Running)
            {
                cooldown += Time.deltaTime;
                if (cooldown >= MaxCooldown)
                {
                    spawnable = true;
                }
            }
        }

        public void Spawn()
        {
            if(spawnable)
            {
                spawnable = false;
                cooldown = 0;
                Instantiate(SpawnObject, SpawnPosition.position, Quaternion.identity, null);
            }
        }

        // Diese Funktion wird aufgerufen, wenn "MonoBehaviour" zerstört wird
        private void OnDestroy()
        {
            LevelManager.instance.RemoveSpawner(gameObject);
        }
    }
}
