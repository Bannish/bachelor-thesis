﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace de.jniederm.bachelor.game
{
    public class ReceiverTrigger : MonoBehaviour
    {
        public bool IsTriggered { get; private set; } = false;
        public BlockMovement Product { get; private set; } = null;


        // "OnTriggerExit" wird aufgerufen, wenn ein "Collider other" aufgehört hat, den Trigger zu berühren
        private void OnTriggerExit(Collider other)
        {
            IsTriggered = false;
            Product = null;
        }

        // "OnTriggerEnter" wird aufgerufen, wenn ein "Collider other" in den Trigger eingetreten ist
        private void OnTriggerEnter(Collider other)
        {
            var obj = other.gameObject;
            
            if (obj.CompareTag("BuildBlock"))
            {
                Product = obj.GetComponent<BlockMovement>();
                IsTriggered = true;
                Debug.Log("found block");
            }
        }
    }
}
