﻿using UnityEngine;

namespace de.jniederm.bachelor.game
{
    public class ReceiverBaseController : MonoBehaviour
    {
        public bool Finished { get; private set; }

        public AudioClip sfxSuccess;
        public AudioClip sfxFail;
        public int ObjectsNeeded = 5;

        public int ObjectsLeft;
        protected AudioSource sfx;


        // Start is called before the first frame update
        public virtual void Start()
        {
            LevelManager.instance.AddReceiver(gameObject);
            ObjectsNeeded = LevelManager.instance.ProductsNeeded;
            ObjectsLeft = ObjectsNeeded;
            sfx = GetComponent<AudioSource>();
            LevelManager.OnRunningChanged += ResetObjectsNeeded;
        }

        // Update is called once per frame
        public virtual void Update()
        {
            if (ObjectsLeft == 0)
            {
                Finished = true;
            }
        }

        // Diese Funktion wird aufgerufen, wenn "MonoBehaviour" zerstört wird
        private void OnDestroy()
        {
            LevelManager.OnRunningChanged -= ResetObjectsNeeded;
            LevelManager.instance.RemoveReceiver(gameObject);
        }

        // Setzt die Anzahl der benötigten Produkte zurück, wenn der Playmode gestartet wird.
        private void ResetObjectsNeeded()
        {
            if (LevelManager.instance.Running)
            {
                ObjectsLeft = ObjectsNeeded;
            }
        }
    }
}
