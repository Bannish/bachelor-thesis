﻿using UnityEngine;

namespace de.jniederm.bachelor.game
{
    public class ReceiverOneBlockController : ReceiverBaseController
    {
        // Start is called before the first frame update
        public override void Start()
        {
            base.Start();
        }

        // Update is called once per frame
        public override void Update()
        {
            base.Update();
        }

        // "OnTriggerStay" wird einmal pro Bild für jedes "Collider" aufgerufen, das den Trigger berührt
        private void OnTriggerStay(Collider other)
        {
            var obj = other.gameObject;
            if (obj.CompareTag("BuildBlock") && !obj.GetComponent<BlockMovement>().Moving)
            {
                Debug.Log("Product Correct");
                Destroy(obj);
                ObjectsLeft -= 1;
                //sfx.PlayOneShot(sfxSuccess);
            }
            else if (obj.CompareTag("BuildBlock") && obj.GetComponent<BlockMovement>().Moving)
            {
                Debug.Log("Waiting for movement to stop");
            }
            else
            {
                Debug.Log("Product not Received");
                //sfx.PlayOneShot(sfxFail);
            }
        }
    }
}
