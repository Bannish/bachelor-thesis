﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace de.jniederm.bachelor.game
{
    public class ReceiverTwoBlockController : ReceiverBaseController
    {
        [Header("ColliderObjects")]
        public List<ReceiverTrigger> triggers; 

        // Start is called before the first frame update
        public override void Start()
        {
            base.Start();
        }

        // Update is called once per frame
        public override void Update()
        {
            int triggered = 0;
            foreach (var receive in triggers)
            {
                if(receive.IsTriggered)
                {
                    triggered++;
                }
            }
            if (triggers[0].Product != null)
            {
                Debug.Log("Product found");
                if (triggered >= 2)
                {
                    Debug.Log("both triggered");
                    if (!triggers[0].Product.Moving || !triggers[1].Product.Moving)
                    {
                        Debug.Log("Not Moving");
                        Destroy(triggers[0].Product.transform.root.gameObject);
                        ObjectsLeft -= 1;
                    }
                }
            } 
            base.Update();
        }
    }
}
