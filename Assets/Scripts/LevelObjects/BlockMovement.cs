﻿using de.jniederm.bachelor.game;
using System.Collections;
using UnityEngine;

namespace de.jniederm.bachelor
{
    [RequireComponent(typeof(AudioSource))]
    public class BlockMovement : MonoBehaviour
    {
        public bool Moving = false;

        public bool Rotating { get; set; } = false;

        public bool Rotated { get; set; } = false;

        public bool Blocked { get; set; } = false;

        private AudioSource sfx;
        private Rigidbody rb;

        // Start is called before the first frame update
        private void Start()
        {
            sfx = GetComponent<AudioSource>();
            rb = GetComponent<Rigidbody>();
        }

        // Update is called once per frame
        private void Update()
        {

        }

        private void OnCollisionEnter(Collision collision)
        {
            GameObject obj = collision.gameObject;
            if (obj.CompareTag("Ground"))
            {
                sfx.Play();
                Destroy(gameObject);
                LevelManager.instance.StopPlayMode();
            }
        }

        // Coroutine, welche die bewegung über eine gewisse zeit hinweg ausführt
        private IEnumerator MoveOverTime(Vector3 newPos, float timeToMove)
        {
            rb = GetComponent<Rigidbody>();
            rb.isKinematic = true;
            var currentPos = transform.position;
            var t = 0f;
            while (t < 1)
            {
                t += Time.deltaTime / timeToMove;
                transform.position = Vector3.Lerp(currentPos, newPos, t);
                if (transform.position == newPos)
                {
                    Moving = false;
                }
                yield return null;
            }
            Moving = false;
            rb.isKinematic = false; ;
        }

        // Coroutine, welche die drehung über eine gewisse zeit hinweg ausführt
        private IEnumerator RotateOverTime(Quaternion newRot, Vector3 newPos, float timeToMove)
        {
            rb = GetComponent<Rigidbody>();
            rb.isKinematic = true;
            var currentRotation = transform.localRotation;
            Quaternion targetRotation = currentRotation * newRot;
            var t = 0f;
            while (t < 1)
            {
                t += Time.deltaTime / timeToMove;
                transform.localRotation = Quaternion.Lerp(currentRotation, targetRotation, t);
                if (transform.localRotation == targetRotation)
                {
                    Rotating = false;
                }
                yield return null;
            }
            Rotating = false;
            rb.isKinematic = false;
            StartCoroutine(MoveOverTime(newPos, timeToMove));
        }

        // Bewegt den Block zu einer bestimmten position
        public void MoveToPosition(Vector3 newPos, float timeToMove = 3.0f)
        {
            Moving = true;
            StartCoroutine(MoveOverTime(newPos, timeToMove));
        }

        // Rotiert den Block um sich selbst in eine bestimmte position
        internal void RotateToPosition(Quaternion newRot, Vector3 newPos, float timeToMove)
        {
            Rotating = true;
            Rotated = true;
            StartCoroutine(RotateOverTime(newRot, newPos, timeToMove));
        }
    }
}
