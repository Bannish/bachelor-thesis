﻿using System;

namespace de.jniederm.bachelor.game.data
{
    [Serializable]
    public class LevelItem
    {
        public string id;
        public string name;
        public string description;
        public string sceneName;
    }
}
