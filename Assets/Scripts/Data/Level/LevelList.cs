﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace de.jniederm.bachelor.game.data
{
    [CreateAssetMenu(fileName = "LevelList.asset", menuName = "jniederm/Create Level List", order = 2)]
    public class LevelList : ScriptableObject, IList<LevelItem>, IDictionary<string, LevelItem>, ISerializationCallbackReceiver
    {
        public List<LevelItem> Levels;

        IDictionary<string, LevelItem> LevelDictionary;

        public LevelItem this[int index] { get => ((IList<LevelItem>)Levels)[index]; set => ((IList<LevelItem>)Levels)[index] = value; }
        public LevelItem this[string key] { get => LevelDictionary[key]; set => LevelDictionary[key] = value; }

        public int Count => ((IList<LevelItem>)Levels).Count;

        public bool IsReadOnly => ((IList<LevelItem>)Levels).IsReadOnly;

        public ICollection<string> Keys => LevelDictionary.Keys;

        public ICollection<LevelItem> Values => LevelDictionary.Values;

        public void Add(LevelItem item)
        {
            throw new NotSupportedException("Level List is read only");
        }

        public void Add(string key, LevelItem value)
        {
            throw new NotSupportedException("Level List is read only");
        }

        public LevelItem GetLevelByID(int value)
        {
            return Levels[value];
        }

        public void Add(KeyValuePair<string, LevelItem> item)
        {
            throw new NotSupportedException("Level List is read only");
        }

        public void Clear()
        {
            throw new NotSupportedException("Level List is read only");
        }

        public bool Contains(LevelItem item)
        {
            return ((IList<LevelItem>)Levels).Contains(item);
        }

        public bool Contains(KeyValuePair<string, LevelItem> item)
        {
            return LevelDictionary.Contains(item);
        }

        public bool ContainsKey(string key)
        {
            return LevelDictionary.ContainsKey(key);
        }

        public void CopyTo(LevelItem[] array, int arrayIndex)
        {
            ((IList<LevelItem>)Levels).CopyTo(array, arrayIndex);
        }

        public void CopyTo(KeyValuePair<string, LevelItem>[] array, int arrayIndex)
        {
            LevelDictionary.CopyTo(array, arrayIndex);
        }

        public IEnumerator<LevelItem> GetEnumerator()
        {
            return ((IList<LevelItem>)Levels).GetEnumerator();
        }

        public int IndexOf(LevelItem item)
        {
            return ((IList<LevelItem>)Levels).IndexOf(item);
        }

        public void Insert(int index, LevelItem item)
        {
            throw new NotSupportedException("Level List is read only");
        }

        public bool Remove(LevelItem item)
        {
            throw new NotSupportedException("Level List is read only");
        }

        public bool Remove(string key)
        {
            throw new NotSupportedException("Level List is read only");
        }

        public bool Remove(KeyValuePair<string, LevelItem> item)
        {
            throw new NotSupportedException("Level List is read only"); ;
        }

        public void RemoveAt(int index)
        {
            throw new NotSupportedException("Level List is read only");
        }

        public bool TryGetValue(string key, out LevelItem value)
        {
            return LevelDictionary.TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IList<LevelItem>)Levels).GetEnumerator();
        }

        IEnumerator<KeyValuePair<string, LevelItem>> IEnumerable<KeyValuePair<string, LevelItem>>.GetEnumerator()
        {
            return LevelDictionary.GetEnumerator();
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            LevelDictionary = Levels.ToDictionary(l => l.id);
        }

        public void OnBeforeSerialize()
        {
        }

    }
}