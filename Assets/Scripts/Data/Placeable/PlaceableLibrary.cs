﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using de.jniederm.bachelor.placeable;
using UnityEngine;

namespace de.jniederm.bachelor.game.data
{
    [CreateAssetMenu(fileName = "PlacableLibrary.asset", menuName = "jniederm/Create Placable Library", order = 1)]
    public class PlaceableLibrary : ScriptableObject, IList<PlaceableItem>, IDictionary<string, PlaceableItem>, ISerializationCallbackReceiver
    {
        public List<PlaceableItem> Placeables;

        IDictionary<string, PlaceableItem> PlaceablesDictionary;

        public PlaceableItem this[int index] { get => ((IList<PlaceableItem>)Placeables)[index]; set => ((IList<PlaceableItem>)Placeables)[index] = value; }
        public PlaceableItem this[string key] { get => PlaceablesDictionary[key]; set => PlaceablesDictionary[key] = value; }

        public int Count => ((IList<PlaceableItem>)Placeables).Count;

        public bool IsReadOnly => ((IList<PlaceableItem>)Placeables).IsReadOnly;

        public ICollection<string> Keys => PlaceablesDictionary.Keys;

        public ICollection<PlaceableItem> Values => PlaceablesDictionary.Values;

        public void Add(PlaceableItem item)
        {
            throw new NotSupportedException("Placeable List is read only");
        }

        public void Add(string key, PlaceableItem value)
        {
            throw new NotSupportedException("Placeable List is read only");
        }

        public void Add(KeyValuePair<string, PlaceableItem> item)
        {
            throw new NotSupportedException("Placeable List is read only");
        }

        public void Clear()
        {
            throw new NotSupportedException("Placeable List is read only");
        }

        public bool Contains(PlaceableItem item)
        {
            return ((IList<PlaceableItem>)Placeables).Contains(item);
        }

        public bool Contains(KeyValuePair<string, PlaceableItem> item)
        {
            return PlaceablesDictionary.Contains(item);
        }

        public bool ContainsKey(string key)
        {
            return PlaceablesDictionary.ContainsKey(key);
        }

        public void CopyTo(PlaceableItem[] array, int arrayIndex)
        {
            ((IList<PlaceableItem>)Placeables).CopyTo(array, arrayIndex);
        }

        public void CopyTo(KeyValuePair<string, PlaceableItem>[] array, int arrayIndex)
        {
            PlaceablesDictionary.CopyTo(array, arrayIndex);
        }

        public IEnumerator<PlaceableItem> GetEnumerator()
        {
            return ((IList<PlaceableItem>)Placeables).GetEnumerator();
        }

        public int IndexOf(PlaceableItem item)
        {
            return ((IList<PlaceableItem>)Placeables).IndexOf(item);
        }

        public void Insert(int index, PlaceableItem item)
        {
            throw new NotSupportedException("Placeable List is read only");
        }

        public void OnAfterDeserialize()
        {
            PlaceablesDictionary = Placeables.ToDictionary(placeable => placeable.id);
        }

        public void OnBeforeSerialize()
        {
        }

        public bool Remove(PlaceableItem item)
        {
            throw new NotSupportedException("Placeable List is read only");
        }

        public bool Remove(string key)
        {
            throw new NotSupportedException("Placeable List is read only");
        }

        public bool Remove(KeyValuePair<string, PlaceableItem> item)
        {
            throw new NotSupportedException("Placeable List is read only");
        }

        public void RemoveAt(int index)
        {
            throw new NotSupportedException("Placeable List is read only");
        }

        public bool TryGetValue(string key, out PlaceableItem value)
        {
            return PlaceablesDictionary.TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IList<PlaceableItem>)Placeables).GetEnumerator();
        }

        IEnumerator<KeyValuePair<string, PlaceableItem>> IEnumerable<KeyValuePair<string, PlaceableItem>>.GetEnumerator()
        {
            return PlaceablesDictionary.GetEnumerator();
        }
    }
}
