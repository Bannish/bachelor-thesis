﻿using de.jniederm.bachelor.placeable;
using System;

namespace de.jniederm.bachelor.game.data
{
    [Serializable]
    public class PlaceableItem
    {
        public string id;
        public string name;
        public PlaceableElement placeable;
    }
}
