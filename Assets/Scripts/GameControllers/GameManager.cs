﻿using de.jniederm.bachelor.game.data;
using System;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

namespace de.jniederm.bachelor.game
{
    public class GameManager : MonoBehaviour
    {
        public LevelList levelList;
        public AudioMixer main;

        public static GameManager instance = null;

        public LevelItem Level { get; set; }

        //Awake is called before any Start functions
        void Awake()
        {
            if (instance == null)
            {
                instance = this;
                SceneManager.sceneLoaded += OnSceneLoaded;
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
        }

        // Diese Funktion wird aufgerufen, wenn "Behaviour" deaktiviert wird oder inaktiv ist
        private void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        // Diese Funktion wird aufgerufen, wenn das Objekt aktiviert wird und aktiv ist
        private void OnEnable()
        {

        }

        //Holt sich beim laden des neuen level die daten für das nächste level
        private void OnSceneLoaded(Scene level, LoadSceneMode loadSceneMode)
        {
            Debug.Log("OnSceneLoaded");
            foreach (var item in levelList)
            {
                if (Convert.ToInt32(item.id) == level.buildIndex && Convert.ToInt32(item.id) < levelList.Count - 1)
                {
                    instance.Level = item;
                    Debug.Log("Found Level: " + instance.Level.sceneName);
                    Debug.Log("Next Level: " + instance.levelList[Convert.ToInt32(instance.Level.id) + 1].sceneName);
                }
            }
        }

        // Wird ausgeführt, wenn das level abgeschlossen ist
        public void LevelCleared()
        {
            if(Convert.ToInt32(instance.Level.id) < levelList.Count - 1)
            {
                SceneManager.LoadSceneAsync(instance.levelList[Convert.ToInt32(instance.Level.id) + 1].sceneName, LoadSceneMode.Single);
            }
            else
            {
                Application.Quit();
            }
        }
    }
}
