﻿using de.jniederm.bachelor.game.data;
using System.Collections.Generic;
using UnityEngine;

namespace de.jniederm.bachelor.game
{
    public class LevelManager : MonoBehaviour
    {
        public PlaceableLibrary Library = null;

        public static LevelManager instance;

        public List<GameObject> Placed = new List<GameObject>();
        public List<GameObject> Spawners = new List<GameObject>();
        public List<GameObject> Receivers = new List<GameObject>();

        public float SpawnDelay = 15f;
        public int ProductsNeeded = 5;

        public bool Running = false;

        public delegate void RunningChanged();
        public static event RunningChanged OnRunningChanged;

        private float cooldown = 0f;

        // Start is called before the first frame update
        void Start()
        {
            cooldown = SpawnDelay;
        }

        // Update is called once per frame
        void Update()
        {
            if (Running)
            {
                cooldown += Time.deltaTime;
                if (cooldown >= SpawnDelay)
                {
                    foreach (var item in Spawners)
                    {
                        item.GetComponent<BlockSpawner>().Spawn();
                    }
                }
            }

            if (Receivers.Count > 0)
            {
                foreach (var item in Receivers)
                {
                    if (!item.GetComponent<ReceiverBaseController>().Finished)
                    {
                        break;
                    }
                    else
                    {
                        GameManager.instance.LevelCleared();
                    }
                }
            }
        }

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }
        }

        #region Add und Remove operationen für die listen, in denen die wichtigsten elemente gespeichert werden.
        public bool AddPlaceable(GameObject element)
        {
            Placed.Add(element);
            return Placed.Contains(element);
        }

        public bool RemovePlaceable(GameObject element)
        {
            return Placed.Remove(element);
        }

        public bool AddSpawner(GameObject element)
        {
            Spawners.Add(element);
            return Spawners.Contains(element);
        }

        public bool RemoveSpawner(GameObject element)
        {
            return Spawners.Remove(element);
        }

        public bool AddReceiver(GameObject element)
        {
            Receivers.Add(element);
            return Receivers.Contains(element);
        }

        public bool RemoveReceiver(GameObject element)
        {
            return Receivers.Remove(element);
        }
        #endregion Add und Remove operationen für die listen, in denen die wichtigsten elemente gespeichert werden.

        public void ResetLevel()
        {
            Running = false;
            foreach (GameObject element in Placed)
            {
                Destroy(element, 1f);
            }
        }

        // Startet das spawnen der Blöcke
        public void PlayMode()
        {
            Running = true;
            OnRunningChanged?.Invoke();
        }

        // Löscht alle blöcke und stoppt das spawnen der blöcke
        public void StopPlayMode()
        {
            Running = false;
            cooldown = SpawnDelay;
            OnRunningChanged?.Invoke();
            var gameObjects = FindObjectsOfType<BlockMovement>();
            foreach (var element in gameObjects)
            {
                Destroy(element.gameObject, 0.5f);
            }
        }
    }
}
