﻿using UnityEngine;

namespace de.jniederm.bachelor.interfaces
{
    public interface IReceiveSensorSignal
    {
        void ReceiveSignal(GameObject obj, bool signal = false);
    }
}