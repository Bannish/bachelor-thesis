﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace de.jniederm.bachelor.placeable
{
    public class RotatorController : PlaceableElement
    {

        public float TimeToMove = 1.0f;
        // Start is called before the first frame update
        public override void Start()
        {
            base.Start();
        }

        // Update is called once per frame
        public override void Update()
        {
            base.Update();
        }

        private void OnCollisionStay(Collision collision)
        {
            GameObject col = collision.gameObject;
            if (col.CompareTag("BuildBlock"))
            {
                BlockMovement block = (BlockMovement)col.GetComponent(typeof(BlockMovement));
                if (!block.Rotating && !block.Rotated && !block.Moving)
                {
                    if (gameObject.CompareTag("RotatorRight"))
                    {
                        Vector3 newPos = transform.position + transform.TransformDirection(new Vector3(0, 1, 1));
                        Quaternion newRot = Quaternion.Euler(0, 90f, 0);
                        block.RotateToPosition(newRot, newPos, TimeToMove);
                    }
                    else if (gameObject.CompareTag("RotatorLeft"))
                    {
                        Vector3 newPos = transform.position + transform.TransformDirection(new Vector3(0, 1, 1));
                        Quaternion newRot = Quaternion.Euler(0, -90f, 0);
                        block.RotateToPosition(newRot, newPos, TimeToMove);
                    }

                }
            }
        }

        private void OnCollisionExit(Collision collision)
        {
            GameObject col = collision.gameObject;
            if (col.CompareTag("BuildBlock"))
            {
                BlockMovement block = (BlockMovement)col.GetComponent(typeof(BlockMovement));
                block.Rotating = false;
                block.Rotated = false;
            }
        }
    }
}
