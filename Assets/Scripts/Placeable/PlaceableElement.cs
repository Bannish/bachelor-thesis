﻿using de.jniederm.bachelor.game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace de.jniederm.bachelor.placeable
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(AudioSource))]
    public class PlaceableElement : MonoBehaviour
    {

        public bool LevelObject = false;

        // Start is called before the first frame update
        public virtual void Start()
        {
            var rb = GetComponent<Rigidbody>();
            rb.isKinematic = true;
            rb.useGravity = false;
            if (!LevelObject)
            {
                LevelManager.instance.AddPlaceable(gameObject);
            }
        }

        // Update is called once per frame
        public virtual void Update()
        {

        }

        public virtual void Remove()
        {
            Debug.Log("calledRemove");
            if(!LevelObject)
            {
                GetComponent<AudioSource>().Play();
                LevelManager.instance.RemovePlaceable(gameObject);
                Destroy(gameObject);
            }
        }

        private void OnDestroy()
        {
            LevelManager.instance.RemovePlaceable(gameObject);
        }
    }
}
