﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace de.jniederm.bachelor.placeable
{
    public class SensorController : MonoBehaviour
    {

        public bool BlockDetected { get; private set; }

        private void OnTriggerEnter(Collider other)
        {
            GameObject col = other.gameObject;
            if (col.CompareTag("BuildBlock"))
            {
                BlockDetected = true;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            GameObject col = other.gameObject;
            if (col.CompareTag("BuildBlock"))
            {
                BlockDetected = false;
            }
        }
    }
}