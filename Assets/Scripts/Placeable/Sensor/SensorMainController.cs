﻿using UnityEngine;

namespace de.jniederm.bachelor.placeable
{
    public class SensorMainController : PlaceableElement
    {
        public bool BlockDetected;
        public SensorController DetectedLeft;
        public SensorController DetectedRight;
        public GameObject receiver;

        // Start is called before the first frame update
        public override void Start()
        {
            base.Start();
        }

        // Update is called once per frame
        public override void Update()
        {
            base.Update();
            bool tmp = BlockDetected;
            BlockDetected = !DetectedLeft.BlockDetected || !DetectedRight.BlockDetected ? false : true;
            if (tmp != BlockDetected)
            {
                SendSignal(BlockDetected);
            }
        }

        private void SendSignal(bool signal)
        {
            receiver.GetComponent<interfaces.IReceiveSensorSignal>().ReceiveSignal(gameObject, signal);
        }

        private void OnTriggerEnter(Collider other)
        {
            GameObject obj = other.gameObject;
            if (!obj.CompareTag("Cable") && !obj.CompareTag("Piston") && !obj.CompareTag("CableEnd"))
            {
                return;
            }
            receiver = obj;
        }

        private void OnTriggerExit(Collider other)
        {
            GameObject obj = other.gameObject;
            if (!obj.CompareTag("Cable") && !obj.CompareTag("Piston"))
            {
                return;
            }
            receiver = null;
            Debug.Log("Disconnected from " + obj.tag);
        }

    }
}
