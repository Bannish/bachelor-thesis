﻿using UnityEngine;

namespace de.jniederm.bachelor.placeable
{
    public class WelderController : PlaceableElement
    {
        public WelderProductFinder LeftProduct;
        public WelderProductFinder RightProduct;

        private bool joining = false;

        // Start is called before the first frame update
        public override void Start()
        {
            base.Start();
        }

        // Update is called once per frame
        public override void Update()
        {
            base.Update();

            if(LeftProduct.hasProduct && RightProduct.hasProduct)
            {
                if(!joining && LeftProduct.product.GetComponent<BlockMovement>() != null && !LeftProduct.product.GetComponent<BlockMovement>().Moving && !RightProduct.product.GetComponent<BlockMovement>().Moving)
                {
                    joining = true;
                    JoinProducts(LeftProduct.product, RightProduct.product);
                }
            }
            else
            {
                joining = false;
            }
        }

        private void JoinProducts(GameObject product1, GameObject product2)
        {
            product1.transform.SetParent(product2.transform.root);
            Destroy(product1.GetComponent<Rigidbody>());
        }
    }
}