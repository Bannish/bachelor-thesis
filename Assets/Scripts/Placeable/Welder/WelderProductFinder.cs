﻿using UnityEngine;

namespace de.jniederm.bachelor.placeable
{
    public class WelderProductFinder : MonoBehaviour
    {

        public GameObject product { get; private set; } = null;
        public bool hasProduct { get; private set; } = false;

        // "OnTriggerEnter" wird aufgerufen, wenn ein "Collider other" in den Trigger eingetreten ist
        private void OnTriggerEnter(Collider other)
        {
            var obj = other.gameObject;
            if (obj.CompareTag("BuildBlock"))
            {
                hasProduct = true;
                product = obj.transform.root.gameObject;
            }
        }

        // "OnTriggerStay" wird einmal pro Bild für jedes "Collider" aufgerufen, das den Trigger berührt
        private void OnTriggerStay(Collider other)
        {
            var obj = other.gameObject;
            if (obj.CompareTag("BuildBlock") && obj.transform.root.gameObject != product)
            {
                return;
            }
            else
            {
                hasProduct = true;
                product = obj.transform.root.gameObject;
            }
        }

        // "OnTriggerExit" wird aufgerufen, wenn ein "Collider other" aufgehört hat, den Trigger zu berühren
        private void OnTriggerExit(Collider other)
        {
            product = null;
            hasProduct = false;
        }
    }
}
