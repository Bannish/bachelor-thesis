﻿using UnityEngine;

namespace de.jniederm.bachelor.placeable
{
    public class ConveyorController : PlaceableElement
    {

        public float TimeToMove = 3.0f;

        public bool Blocked { get; private set; } = false;

        private void OnCollisionStay(Collision collision)
        {
            GameObject col = collision.gameObject;
            if (col.CompareTag("BuildBlock"))
            {
                BlockMovement block = (BlockMovement)col.GetComponent(typeof(BlockMovement));
                if (Blocked)
                {
                    block.Blocked = true;
                }
                else
                {
                    if ((!block.Rotating && !block.Rotated && !block.Moving) || (!block.Rotating && !block.Moving && block.Rotated))
                    {
                        Vector3 newPos = transform.position + transform.TransformDirection(new Vector3(0, 1, 1.01f));
                        block.Blocked = false;
                        block.MoveToPosition(newPos, TimeToMove);
                    }
                }
            }
        }

        private void OnTriggerStay(Collider other)
        {

            GameObject obj = other.gameObject;
            if (!obj.CompareTag("Piston"))
            {
                if (obj.CompareTag("BuildBlock") && obj.GetComponentInParent<BlockMovement>() != null)
                {
                    BlockMovement block = obj.GetComponentInParent<BlockMovement>();
                    Blocked = block.Blocked ? true : false;
                }
            }
            else
            {
                Debug.Log("trigger ausgelöst");
                Blocked = true;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            GameObject obj = other.gameObject;
            if (obj.CompareTag("Piston") || obj.CompareTag("BuildBlock"))
            {
                Blocked = false;
            }
        }
    }
}
