﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace de.jniederm.bachelor.placeable
{
    public class ShaftMovement : MonoBehaviour
    {
        public bool Moving = false;

        public float MoveTime = 1.5f;

        // "Start" wird aufgerufen, bevor eine der "Update"-Methoden erstmalig aufgerufen wird
        private void Start()
        {
            var rb = GetComponent<Rigidbody>();
            rb.isKinematic = true;
            rb.useGravity = false;
        }

        public void MoveToPosition(Vector3 newPos, float timeToMove = 1.5f)
        {
            Moving = true;
            StartCoroutine(MoveOverTime(newPos, timeToMove));
        }

        private IEnumerator MoveOverTime(Vector3 newPos, float timeToMove)
        {
            var currentPos = transform.localPosition;
            var t = 0f;
            while (t < 1)
            {
                t += Time.deltaTime / timeToMove;
                transform.localPosition = Vector3.Lerp(currentPos, newPos, t);
                if (transform.localPosition == newPos)
                {
                    Moving = false;
                }
                yield return null;
            }
        }

        private void OnCollisionStay(Collision collision)
        {
            GameObject obj = collision.gameObject;
            
            if (obj.CompareTag("BuildBlock"))
            {
                BlockMovement block = (BlockMovement)obj.GetComponentInParent(typeof(BlockMovement));
                if (!block.Moving && !block.Rotating && !block.Blocked && Moving)
                {
                    Vector3 newPos = transform.TransformPoint(2, 0, 0);
                    block.Blocked = false;
                    block.Moving = true;
                    block.MoveToPosition(newPos, MoveTime);
                }
            }
        }
    }
}


