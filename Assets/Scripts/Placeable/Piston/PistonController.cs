﻿using de.jniederm.bachelor.game;
using UnityEngine;

namespace de.jniederm.bachelor.placeable
{
    public class PistonController : PlaceableElement, interfaces.IReceiveSensorSignal
    {
        public bool ReceivedSignal { get; private set; }

        public ShaftMovement Shaft;

        public float TimeToMove = 1.5f;

        public bool Extended { get; set; }

        // "Start" wird aufgerufen, bevor eine der "Update"-Methoden erstmalig aufgerufen wird
        public override void Start()
        {
            if (!LevelObject)
            {
                LevelManager.instance.AddPlaceable(gameObject);
            }
        }

        // Update is called once per frame
        public override void Update()
        {
            base.Update();
            if (ReceivedSignal && !Extended && !Shaft.Moving)
            {
                Extended = true;
                Shaft.MoveToPosition(new Vector3(1f, 0, 0), TimeToMove);
            }
            else if (!ReceivedSignal && Extended && !Shaft.Moving)
            {
                Extended = false;
                Shaft.MoveToPosition(new Vector3(0, 0, 0), TimeToMove);
            }
        }

        public void ReceiveSignal(GameObject obj, bool signal = false)
        {
            ReceivedSignal = signal;
        }
    }
}

