﻿using UnityEngine;

namespace de.jniederm.bachelor.placeable
{
    public class CableEndController : MonoBehaviour, interfaces.IReceiveSensorSignal
    {

        public bool IsConnected { get; private set; }
        public bool ReceivedSignal { get; private set; }

        public GameObject ConnectedTo;
        private Renderer Renderer;


        // Start is called before the first frame update
        private void Start()
        {
            Renderer = gameObject.GetComponent<Renderer>();
            Renderer.enabled = false;
        }

        // Update is called once per frame
        private void Update()
        {

        }

        private void OnTriggerEnter(Collider collider)
        {
            GameObject obj = collider.gameObject;
            if (!obj.CompareTag("Piston") && !obj.CompareTag("Sensor") && !obj.CompareTag("Cable") && !obj.CompareTag("CableEnd"))
            {
                return;
            }
            IsConnected = true;
            ConnectedTo = obj;
            Renderer.enabled = IsConnected;
        }

        private void OnTriggerExit(Collider collider)
        {
            GameObject obj = collider.gameObject;
            if (!obj.CompareTag("Piston") && !obj.CompareTag("Sensor") && !obj.CompareTag("Cable") && !obj.CompareTag("CableEnd"))
            {
                return;
            }
            IsConnected = false;
            ConnectedTo = null;
            Renderer.enabled = IsConnected;
        }

        public void ReceiveSignal(GameObject obj, bool signal = false)
        {
            ReceivedSignal = signal;
            if (obj.CompareTag("Cable"))
            {
                if (ConnectedTo != null)
                {
                    ConnectedTo.GetComponent<interfaces.IReceiveSensorSignal>().ReceiveSignal(gameObject, signal);
                }
            }
            else
            {
                GetComponentInParent<CableController>().GetSignal(gameObject, signal);
            }
        }
    }
}
