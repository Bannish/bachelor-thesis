﻿using System.Collections.Generic;
using UnityEngine;

namespace de.jniederm.bachelor.placeable
{
    public class CableController : PlaceableElement
    {
        public List<CableEndController> cableEnds;

        public void GetSignal(GameObject obj, bool signal = false)
        {
            foreach (CableEndController cableEnd in cableEnds)
            {
                if (cableEnd.gameObject != obj)
                {
                    cableEnd.ReceiveSignal(gameObject, signal);
                }
            }

        }

        // Start is called before the first frame update
        public override void Start()
        {
            base.Start();
        }

        // Update is called once per frame
        public override void Update()
        {
            base.Update();
        }
    }
}
